#  BestPlaces

### Author
Piotr Wadowski

### Description
Simple app where user can store his favourite places with photos, description and localisation.

### Dependecies
- RealmSwift

Nearly all graphics are from [Icons8](https://icons8.com).
Globe icon made by [Smashicons](https://www.flaticon.com/authors/smashicons) and downloaded from [Flaticon](https://www.flaticon.com/).
