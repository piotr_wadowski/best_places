//
//  DataAccessPlace.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 23.12.2017.
//  Copyright © 2017 Piotr Wadowski. All rights reserved.
//

import Foundation
import RealmSwift

public class DataAccessPlace {
    
    private let database: Realm
    
    public var places: [Place] {
        return Array(database.objects(Place.self).sorted(byKeyPath: "idx"))
    }
    
    public var placesWithLocation: [Place] {
        return Array(database.objects(Place.self).filter("_latitude != nil AND _longitude != nil") )
    }
    
    public init() {
        self.database = try! Realm()
    }
    
    public func createPlace(name: String) -> Place {
        let idx = database.objects(Place.self).count
        return Place(idx: idx, name: name)
    }
    
    public func movePlace(_ place: Place, newIdx: Int) {
        if newIdx < 0 || newIdx >= database.objects(Place.self).count {
             print("🛑 - Index out of bounds")
            return
        }
        if !isPlaceExists(place: place) {
            print("⚠️ - Place doesn't exist. Use savePlace first.")
            return
        }
        let oldIdx = place.idx
        var placesToUpdate = database.objects(Place.self)
        if newIdx < oldIdx {    //moving up
            placesToUpdate = placesToUpdate.filter("idx >= \(newIdx) AND idx < \(oldIdx)")
        } else {
            placesToUpdate = placesToUpdate.filter("idx > \(oldIdx) AND idx <= \(newIdx)")
        }
        do {
            try database.write {
                placesToUpdate.forEach {
                    $0.idx -= (place.idx > newIdx) ? -1 : 1
                }
                place.idx = newIdx
            }
        } catch {
            print("🛑 - " + error.localizedDescription)
            return
        }
    }
    
    public func savePlace(_ place: Place) {
        if !isPlaceExists(place: place) {
            if let webpage = place.webPage, !webpage.contains("http://") && !webpage.contains("https://") {
                place.webPage = "http://" + webpage
            }
            do {
                try database.write {
                    database.add(place)
                }
            } catch {
                print("🛑 - " + error.localizedDescription)
                return
            }
        } else {
            print("⚠️ - Place alreday exists. Use updatePlace instead.")
        }
    }
    
    public func updatePlace(_ place: Place, updateBlock: (Place) -> Void) {
        if isPlaceExists(place: place) {
            do {
                try database.write {
                    updateBlock(place)
                }
            } catch {
                print("🛑 - " + error.localizedDescription)
                return
            }
        } else {
            print("⚠️ - Place doesn't exist. Use savePlace first.")
        }
    }
    
    public func deletePlace(_ place: Place) {
        if isPlaceExists(place: place) {
            for imageName in place.imageNames {
                Tools.removeImage(name: imageName)
            }
            for placeToEdit in database.objects(Place.self).filter("idx > \(place.idx)") {
                updatePlace(placeToEdit, updateBlock: { (editingPlace) in
                    editingPlace.idx -= 1
                })
            }
            do {
                try database.write {
                    database.delete(place)
                }
            } catch {
                print("🛑 - " + error.localizedDescription)
                return
            }
        } else {
            print("⚠️ - Place doesn't exist")
        }
    }
    
    private func isPlaceExists(place: Place) -> Bool {
        let predicate = NSPredicate(format: "idx == \(place.idx)")
        return !database.objects(Place.self).filter(predicate).isEmpty
    }
}
