//
//  Place.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 23.12.2017.
//  Copyright © 2017 Piotr Wadowski. All rights reserved.
//

import Foundation
import RealmSwift

public class Place: Object {
    
    @objc private dynamic var key: String = UUID().uuidString
    @objc dynamic var idx: Int = -1
    @objc dynamic var isFavourite = false
    @objc dynamic var name = ""
    @objc dynamic var webPage: String? = nil
    @objc dynamic var note: String? = nil
    private var _longitude = RealmOptional<Double>()
    private var _latitude = RealmOptional<Double>()
    let imageNames = List<String>()
    
    var longitude: Double? {
        get { return _longitude.value }
        set { _longitude.value = newValue }
    }
    
    var latitude: Double? {
        get { return _latitude.value }
        set { _latitude.value = newValue }
    }
    
    convenience init(idx: Int, name: String) {
        self.init()
        self.idx = idx
        self.name = name
    }
    
    override public static func primaryKey() -> String {
        return "key"
    }
    
}
