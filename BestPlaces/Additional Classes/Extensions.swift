//
//  Extensions.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 03.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func makeBlurEffect(style: UIBlurEffectStyle) {
        self.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: style)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = self.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.insertSubview(blurView, at: 0)
    }
    
}

extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return Theme.current.statusBarStyle
    }
    
}

extension UIColor {
    
    convenience init?(hex hexStr: String, alpha: CGFloat = 1.0) {
        let startIndex = hexStr.startIndex
        guard hexStr[startIndex] == "#" && hexStr.count == 7 else {
            return nil
        }
        if let hex = Int(hexStr[hexStr.index(after: startIndex)...], radix: 16) {
            let red = CGFloat(hex >> 16) / 0xff
            let green = CGFloat((hex & 0x00ff00) >> 8) / 0xff
            let blue = CGFloat((hex & 0x0000ff)) / 0xff
            self.init(red: red, green: green, blue: blue, alpha: alpha)
        } else {
            return nil
        }
    }
    
}
