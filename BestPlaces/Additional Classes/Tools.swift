//
//  Tools.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 23.12.2017.
//  Copyright © 2017 Piotr Wadowski. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Tools {
    
    static func readJPG(name: String) -> UIImage? {
        let filename = getImageURL(name: name)
        let fm = FileManager.default
        if !fm.fileExists(atPath: filename.path) {
            return nil
        }
        if let data = fm.contents(atPath: filename.path) {
            return UIImage(data: data)
        } else {
            return nil
        }
    }
    
    static func saveToJPG(image: UIImage, name: String) {
        if let data = UIImageJPEGRepresentation(image, 0.8) {
            do {
                try data.write(to: getImageURL(name: name))
            } catch {
                print("🛑 - Couldn't save image - \(error.localizedDescription)")
            }
        }
    }
    
    static func removeImage(name: String) {
        let fm = FileManager.default
        do {
            try fm.removeItem(at: getImageURL(name: name))
        } catch {
            print("🛑 - Couldn't remove image - \(error.localizedDescription)")
        }
    }
    
    static func createLocationString(placemark: CLPlacemark) -> String {
        var locStr = ""
        if let street = placemark.thoroughfare {
            locStr += "\(street)"
            if let number = placemark.subThoroughfare {
                locStr += " \(number), "
            }
//            locStr += "\n"
        }
        if let city = placemark.locality {
            locStr += "\(city), "
        }
        if let country = placemark.country {
            locStr += country
        }
        return locStr
    }
    
    private static func getImageURL(name: String) -> URL {
        return getDocumentDirectory().appendingPathComponent("\(name).jpeg")
    }
    
    private static func getDocumentDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDir = paths[0]
        return docDir
    }
    
}
