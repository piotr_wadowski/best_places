//
//  Theme.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 12.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import UIKit

enum Theme: Int {
    case standard
    
    private struct Constants {
        static let savedTheme = "savedTheme"
    }
    
    static var current: Theme {
        let savedTheme = UserDefaults.standard.integer(forKey: Constants.savedTheme)
        return Theme(rawValue: savedTheme) ?? .standard
    }
    
    var statusBarStyle: UIStatusBarStyle {
        switch self {
        case .standard:
            return .lightContent
        }
    }
    
    var logoText: NSAttributedString {
        let text = NSAttributedString(string: "Best Places", attributes: [
            NSAttributedStringKey.font: UIFont(name: "Noteworthy-Bold", size: 30.0)!,
            NSAttributedStringKey.foregroundColor: UIColor.white
            ])
        return text
    }
    
    var mainColor: UIColor {
        switch self {
        case .standard:
            return UIColor(hex: "#ff9900")!
        }
    }
    
    var itemsColor: UIColor {
        switch self {
        case .standard:
            return UIColor.white
        }
    }
    
    func apply() {
        UserDefaults.standard.set(rawValue, forKey: Constants.savedTheme)
        
        let navBar = UINavigationBar.appearance()
        navBar.isTranslucent = false
        navBar.barTintColor = mainColor
        navBar.tintColor = itemsColor
        navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : itemsColor]
        
        let tabBar = UITabBar.appearance()
        tabBar.isTranslucent = false
        tabBar.barTintColor = mainColor
        tabBar.tintColor = itemsColor
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        
    }
}
