//
//  ImagePresenterViewController.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 05.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

class ImagePresenterViewController: UIViewController {
    
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var deleteButton: UIButton!
    @IBOutlet private weak var pageControl: UIPageControl!
    @IBOutlet private weak var pagingView: UIScrollView!
    
    private var currentPage = 0
    
    var sourceVC: UIViewController?
    var place: Place!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard place != nil else {
            self.dismiss(animated: true, completion: nil)
            return
        }
    
        preparePagingView()
        prepareButtons()
        
        let hideGesture = UISwipeGestureRecognizer(target: self, action: #selector(hideController))
        hideGesture.direction = .down
        hideGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(hideGesture)
    }
    
    //MARK: - Private methods
    
    private func preparePagingView() {
        pagingView.delegate = self
        pagingView.isPagingEnabled = true
        pagingView.bounces = true
        pagingView.showsHorizontalScrollIndicator = false
        loadPagingView()
    }
    
    private func loadPagingView() {
        pagingView.subviews.forEach { $0.removeFromSuperview() }
        let pageWidth = pagingView.frame.width
        let pageHeight = pagingView.frame.height
        for (idx, imageName) in place.imageNames.enumerated() {
            let imageView = UIImageView(frame: CGRect(x: pageWidth*CGFloat(idx), y: 0.0, width: pageWidth, height: pageHeight))
            imageView.contentMode = .scaleAspectFit
            imageView.image = Tools.readJPG(name: imageName)
            self.pagingView.addSubview(imageView)
        }
        pagingView.contentSize = CGSize(width: pageWidth*CGFloat(place.imageNames.count), height: pageHeight)
        pagingView.contentOffset.x = pageWidth * CGFloat(currentPage)
        
        pageControl.numberOfPages = place.imageNames.count
        pageControl.currentPage = currentPage
        pageControl.hidesForSinglePage = true
    }

    private func prepareButtons() {
        closeButton.addTarget(self, action: #selector(hideController), for: .touchDown)
        deleteButton.addTarget(self, action: #selector(deleteImage), for: .touchDown)
    }
    
    @objc private func hideController() {
        if let src = sourceVC as? PlaceViewController {
            src.backFromImagePresenter()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func deleteImage() {
        let dataAccess = DataAccessPlace()
        Tools.removeImage(name: place.imageNames[currentPage])
        dataAccess.updatePlace(place) { (place) in
            place.imageNames.remove(at: currentPage)
        }
        if currentPage >= place.imageNames.count {
            currentPage = place.imageNames.count-1
        }
        loadPagingView()
    }
}

extension ImagePresenterViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = pagingView.frame.width
        currentPage = Int(scrollView.contentOffset.x / pageWidth)
        pageControl.currentPage = currentPage
    }
    
}
