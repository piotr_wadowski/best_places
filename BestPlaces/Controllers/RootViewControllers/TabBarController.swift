//
//  TabBarController.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 04.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    private struct TabBarVCConstants {
        
        static let createVCIdentifier = "createViewController"
        
        private init() { }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self

        if let tabs = tabBar.items {
            tabs[0].title = NSLocalizedString("LIST_TAB", comment: "Title of tab with list of places")
            tabs[1].title = NSLocalizedString("ADD_TAB", comment: "Title of tab for adding new place")
            tabs[2].title = NSLocalizedString("MAP_TAB", comment: "Title of tab for map")
        }
    }
}

extension TabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let navCtr = viewController as? UINavigationController, navCtr.topViewController is CreateViewController {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            if let createVC = storyboard.instantiateViewController(withIdentifier: TabBarVCConstants.createVCIdentifier) as? CreateViewController {
                let navigation = UINavigationController(rootViewController: createVC)
                self.present(navigation, animated: true, completion: nil)
            }
            return false
        }
        return true
    }
    
}
