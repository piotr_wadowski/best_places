//
//  ChooseLocationTrigger.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 06.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import CoreLocation

protocol ChooseLocationTrigger {
    
    func returningLocation(coordinates: CLLocationCoordinate2D)
}
