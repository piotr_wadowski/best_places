//
//  ChooseLocationViewController.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 06.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit
import MapKit

class ChooseLocationViewController: UIViewController {

    @IBOutlet private weak var mapView: MKMapView!
    
    var sourceVC: ChooseLocationTrigger?
    var choosedLocation: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard sourceVC != nil else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        prepareNavbar()
        
        if let choosedLocation = choosedLocation {
            addAnnotation(coordinates: choosedLocation)
            let region = MKCoordinateRegion(center: choosedLocation, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            mapView.setRegion(region, animated: false)
        }
        
        let chooseLocationGesture = UILongPressGestureRecognizer(target: self, action: #selector(chooseLocation))
        chooseLocationGesture.minimumPressDuration = 1.0
        mapView.addGestureRecognizer(chooseLocationGesture)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        removeAllAnnotations()
    }
    
    //MARK: - Private Methods
    
    private func prepareNavbar() {
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelController))
        navigationItem.leftBarButtonItem = cancelBtn
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneController))
        navigationItem.rightBarButtonItem = doneBtn
    }

    @objc private func chooseLocation(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            let point = gesture.location(in: mapView)
            let location = mapView.convert(point, toCoordinateFrom: mapView)
            choosedLocation = location
            addAnnotation(coordinates: location)
        }
    }
    
    private func addAnnotation(coordinates: CLLocationCoordinate2D) {
        removeAllAnnotations()
        let anno = MKPointAnnotation()
        anno.coordinate = coordinates
        mapView.addAnnotation(anno)
    }
    
    private func removeAllAnnotations() {
        for anno in mapView.annotations {
            if !(anno is MKUserLocation) {
                mapView.removeAnnotation(anno)
            }
        }
    }
    
    @objc private func doneController() {
        if let sourceVC = sourceVC, let choosedLocation = choosedLocation {
            sourceVC.returningLocation(coordinates: choosedLocation)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func cancelController() {
        self.dismiss(animated: true, completion: nil)
    }
}
