//
//  ListTableViewCell.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 28.12.2017.
//  Copyright © 2017 Piotr Wadowski. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet private weak var sampleImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var webPageLabel: UILabel!
    @IBOutlet private weak var favouriteImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func prepareCell(sampleImage: UIImage?, name: String, webPage: String?, isFavourite: Bool) {
        if sampleImage != nil {
            sampleImageView.layer.cornerRadius = sampleImageView.frame.width / 5.0
            sampleImageView.clipsToBounds = true
            sampleImageView.image = sampleImage
            sampleImageView.layer.borderColor = UIColor.gray.cgColor
            sampleImageView.layer.borderWidth = 1.0
        } else {
            sampleImageView.image = UIImage(named: "place")
        }
        nameLabel.text = name
        webPageLabel.text = (webPage == nil) ? "" : webPage
        if isFavourite {
            favouriteImageView.image = UIImage(named: "favStar")
        } else {
            favouriteImageView.isHidden = true
        }
    }
    
}
