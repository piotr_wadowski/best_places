//
//  ListViewController.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 28.12.2017.
//  Copyright © 2017 Piotr Wadowski. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

    private struct ListVCConst {
        static let cellNibName = "ListTableViewCell"
        static let cellIdentifier = "listCell"
        static let showPlaceSegueIdentifier = "showPlace"
        
        private init() { }
    }
    
    @IBOutlet private weak var listTableView: UITableView!
    
    private var dataSource: DataAccessPlace!
    private var tableData = [Place]()
    private var choosedPlace: Place?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = DataAccessPlace()
        tableData = dataSource.places
        
        navigationItem.rightBarButtonItem = editButtonItem
        let navTitleLabel = UILabel()
        navTitleLabel.attributedText = Theme.current.logoText
        navTitleLabel.textAlignment = .center
        navTitleLabel.numberOfLines = 1
        navigationItem.titleView = navTitleLabel
        
        listTableView.register(UINib(nibName: ListVCConst.cellNibName, bundle: Bundle.main), forCellReuseIdentifier: ListVCConst.cellIdentifier)
        listTableView.rowHeight = 100.0
        listTableView.dataSource = self
        listTableView.delegate = self
        self.tabBarItem.title = NSLocalizedString("LIST_TAB", comment: "Title of tab with list of places")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableData = dataSource.places
        self.listTableView.reloadData()
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ListVCConst.showPlaceSegueIdentifier, let dst = segue.destination as? PlaceViewController, let place = choosedPlace {
            dst.place = place
        }
    }
}

extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.listTableView.setEditing(editing, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListVCConst.cellIdentifier, for: indexPath)
        let place = tableData[indexPath.row]
        
        if let cell = cell as? ListTableViewCell {
            let sampleImage = (place.imageNames.first != nil) ? Tools.readJPG(name: place.imageNames.first!) : nil
            cell.prepareCell(sampleImage: sampleImage, name: place.name, webPage: place.webPage, isFavourite: place.isFavourite)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let place = tableData[sourceIndexPath.row]
        dataSource.movePlace(place, newIdx: destinationIndexPath.row)
        tableData = dataSource.places
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let place = tableData[indexPath.row]
        let isFav = place.isFavourite
        
        let actionTitle = isFav ?
        NSLocalizedString("UNFAV_ACTION", comment: "Title of unfavourite action") :
        NSLocalizedString("FAV_ACTION", comment: "Title of favourite action")
        let favAction = UIContextualAction(style: .normal, title: actionTitle) { (action, view, completionHandler) in
            self.dataSource.updatePlace(place, updateBlock: { (place) in
                place.isFavourite = !isFav
            })
            self.listTableView.reloadRows(at: [indexPath], with: .none)
            completionHandler(true)
        }
        favAction.backgroundColor = UIColor.orange
        favAction.image = UIImage(named: "favStar")
        
        return UISwipeActionsConfiguration(actions: [favAction])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let place = tableData[indexPath.row]
        
        let actionTitle = NSLocalizedString("DELETE_ACTION", comment: "Title of delete action")
        let deleteAction = UIContextualAction(style: .destructive, title: actionTitle) { (action, view, completionHandler) in
            self.dataSource.deletePlace(place)
            self.tableData = self.dataSource.places
            completionHandler(true)
        }
        deleteAction.image = UIImage(named: "trash")
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        choosedPlace = tableData[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: false)
        self.performSegue(withIdentifier: ListVCConst.showPlaceSegueIdentifier, sender: self)
    }
}
