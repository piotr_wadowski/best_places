//
//  PlaceAnnotation.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 05.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class PlaceAnnotation: NSObject, MKAnnotation {
    
    var place: Place
    var coordinate: CLLocationCoordinate2D {
        guard let latitude = place.latitude, let longitude = place.longitude else {
            return CLLocationCoordinate2D()
        }
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    var title: String? {
        return place.name
    }
    var imageName: String? {
        return place.imageNames.first
    }
    
    init(place: Place) {
        self.place = place
        super.init()
    }
}
