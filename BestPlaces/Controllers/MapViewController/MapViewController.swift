//
//  MapViewController.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 04.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    private struct MapVCConstants {
        static let annotationIdentifier = "entryAnnotation"
        static let showPlaceSegueIdentifier = "showPlace"
        
        private init() { }
    }
    
    @IBOutlet private weak var mapView: MKMapView!
    
    private var locationManager: CLLocationManager?
    private var chosenPlace: Place?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupManager()
        mapView.delegate = self
        navigationItem.title = NSLocalizedString("MAP_TITLE", comment: "Title for map controller")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        locationManager?.startUpdatingLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        locationManager?.stopUpdatingLocation()
        
        for anno in mapView.annotations {
            if !(anno is MKUserLocation) {
                mapView.removeAnnotation(anno)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let annotations = DataAccessPlace().placesWithLocation.map { return PlaceAnnotation(place: $0) }
        mapView.addAnnotations(annotations)
    }

    private func setupManager() {
        locationManager = CLLocationManager()
        locationManager!.delegate = self
        locationManager!.desiredAccuracy = kCLLocationAccuracyBest
        locationManager!.requestWhenInUseAuthorization()
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == MapVCConstants.showPlaceSegueIdentifier, let dst = segue.destination as? PlaceViewController {
            dst.place = chosenPlace
        }
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            manager.startUpdatingLocation()
        }
    }
    
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        var dequedView = mapView.dequeueReusableAnnotationView(withIdentifier: MapVCConstants.annotationIdentifier)
        if dequedView == nil {
            dequedView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: MapVCConstants.annotationIdentifier)
        }
        if let markerView = dequedView as? MKMarkerAnnotationView, let annotation = annotation as? PlaceAnnotation {
            let placeName = annotation.place.name
            markerView.glyphText = String(placeName[placeName.startIndex]).uppercased()
            markerView.titleVisibility = .hidden
            markerView.canShowCallout = true
            
            markerView.rightCalloutAccessoryView = UIButton(type: .infoLight)
            
            if let imageName = annotation.imageName, let image = Tools.readJPG(name: imageName) {
                let imageView = UIImageView(image: image)
                imageView.frame = CGRect(x: 5, y: 5, width: markerView.frame.width+15, height: markerView.frame.height+15)
                imageView.clipsToBounds = true
                imageView.contentMode = .scaleAspectFill
                imageView.layer.cornerRadius = imageView.frame.width / 4.0
                markerView.leftCalloutAccessoryView = imageView
            }
        }
        return dequedView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let place = (view.annotation as? PlaceAnnotation)?.place else { return }
        chosenPlace = place
        performSegue(withIdentifier: MapVCConstants.showPlaceSegueIdentifier, sender: self)
    }
}
