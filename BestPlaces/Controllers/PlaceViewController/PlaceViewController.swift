//
//  PlaceViewController.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 03.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit
import MapKit
import SafariServices


class PlaceViewController: UIViewController, EditableController {
    
    private struct PlaceVCConstants {
        
        static let showImageSegueIdentifier = "showImagePresenter"
        static let editPlaceSegueIdentifier = "editPlace"
        
        private init() { }
    }

    @IBOutlet private weak var favouriteImageView: UIImageView!
    @IBOutlet private weak var photoView: UIView!
    @IBOutlet private weak var sampleImageView: UIImageView!
    @IBOutlet private weak var morePhotosView: UIView!
    @IBOutlet private weak var morePhotosCountLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var locationView: UIView!
    @IBOutlet private weak var locationMapView: MKMapView!
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var webpageView: UIView!
    @IBOutlet private weak var linkLabel: UILabel!
    @IBOutlet private weak var noteTextView: UITextView!
    
    var place: Place!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard place != nil else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        favouriteImageView.isHidden = !place.isFavourite
        prepareHeaderView()
        prepareLocationView()
        prepareWebpageView()
        noteTextView.text = place.note != nil ? place.note : ""
        noteTextView.isEditable = false
        
        let editBtn = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editPlace))
        navigationItem.rightBarButtonItem = editBtn
    }

    //MARK: - Public methods for navigation
    
    func backFromImagePresenter() {
        setSampleImageView()
    }
    
    func editEnded() {
        favouriteImageView.isHidden = !place.isFavourite
        prepareHeaderView()
        prepareLocationView()
        prepareWebpageView()
        noteTextView.text = place.note != nil ? place.note : ""
    }
    
    //MARK: - Private methods
    
    private func prepareHeaderView() {
        setSampleImageView()
        nameLabel.numberOfLines = 0
        nameLabel.text = place.name
    }
    
    private func setSampleImageView() {
        if let firstImageName = place.imageNames.first {
            photoView.clipsToBounds = true
            photoView.layer.cornerRadius = photoView.frame.width / 5.0
            photoView.layer.borderWidth = 1.0
            photoView.layer.borderColor = UIColor.gray.cgColor
            photoView.isUserInteractionEnabled = true
            photoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openImagePresenter)))
            sampleImageView.contentMode = .scaleAspectFill
            sampleImageView.image = Tools.readJPG(name: firstImageName)
            if place.imageNames.count > 1 {
                morePhotosView.makeBlurEffect(style: .light)
                morePhotosView.layer.cornerRadius = morePhotosView.frame.height / 4.0
                morePhotosCountLabel.text = "+\(place.imageNames.count-1)"
                morePhotosCountLabel.textColor = UIColor.white
            } else {
                morePhotosView.isHidden = true
            }
        } else {
            sampleImageView.image = UIImage(named: "place")
            morePhotosView.isHidden = true
        }
    }
    
    @objc private func openImagePresenter() {
        self.performSegue(withIdentifier: PlaceVCConstants.showImageSegueIdentifier, sender: self)
    }
    
    private func prepareLocationView() {
        guard let latitude = place.latitude, let longitude = place.longitude else {
            locationView.isHidden = true
            return
        }
        locationMapView.isScrollEnabled = false
        locationMapView.isZoomEnabled = false
        locationMapView.isRotateEnabled = false
        locationMapView.isUserInteractionEnabled = false
        locationMapView.layer.cornerRadius = locationMapView.frame.width / 5.0
        locationMapView.layer.borderWidth = 1.0
        locationMapView.layer.borderColor = UIColor.gray.cgColor
        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        locationMapView.addAnnotation(annotation)
        let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        locationMapView.setRegion(region, animated: false)
        
        locationLabel.text = ""
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { (placemarks, error) in
            if error == nil, let placemark = placemarks?.first {
                self.locationLabel.text = Tools.createLocationString(placemark: placemark)
            }
        }
    }
    
    private func prepareWebpageView() {
        guard let webpageLink = place.webPage else {
            webpageView.isHidden = true
            return
        }
        webpageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openWebpage)))
        linkLabel.attributedText = NSAttributedString(string: webpageLink, attributes: [
            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
            NSAttributedStringKey.foregroundColor: UIColor.blue
            ])
    }
    
    @objc private func openWebpage() {
        guard let webpage = place.webPage, let url = URL(string: webpage) else { return }
        let safariVC = SFSafariViewController(url: url)
        self.present(safariVC, animated: true, completion: nil)
    }
    
    @objc private func editPlace() {
        self.performSegue(withIdentifier: PlaceVCConstants.editPlaceSegueIdentifier, sender: self)
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == PlaceVCConstants.showImageSegueIdentifier, let dst = segue.destination as? ImagePresenterViewController {
            dst.place = place
            dst.sourceVC = self
        } else if segue.identifier == PlaceVCConstants.editPlaceSegueIdentifier, let navDst = segue.destination as? UINavigationController, let dst = navDst.topViewController as? CreateViewController {
            dst.sourceVC = self
            dst.place = place
        }
    }
}
