//
//  PhotoCollectionViewCell.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 06.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var photoImageView: UIImageView!
    
    var image: UIImage? {
        didSet {
            photoImageView.image = image
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
