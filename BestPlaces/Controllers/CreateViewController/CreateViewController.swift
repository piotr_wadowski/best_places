//
//  CreateViewController.swift
//  BestPlaces
//
//  Created by Piotr Wadowski on 04.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit
import CoreLocation
import Photos

class CreateViewController: UIViewController, ChooseLocationTrigger {

    private struct CreateVCConstants {
        
        static let photoCellNibName = "PhotoCollectionViewCell"
        static let photoCellIdentifier = "photoCell"
        static let chooseLocationSegueIdentifier = "chooseLocation"
        
        private init() { }
    }
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var photosCollectionView: UICollectionView!
    @IBOutlet private weak var addPhotoButton: UIButton!
    @IBOutlet private weak var addLocationButton: UIButton!
    @IBOutlet private weak var webpageLabel: UILabel!
    @IBOutlet private weak var webpageTextField: UITextField!
    @IBOutlet private weak var isFavouriteLabel: UILabel!
    @IBOutlet private weak var isFavouriteSwitch: UISwitch!
    @IBOutlet private weak var noteTextView: UITextView!
    
    private var photosNames = [String]()
    private var choosedLocation: CLLocationCoordinate2D?
    private let imagePicker = UIImagePickerController()
    private var notAcceptedPhotosNames = [String]()
    
    var place: Place?
    var sourceVC: EditableController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard (sourceVC != nil && place != nil) || (sourceVC == nil && place == nil) else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        prepareNavBar()
        prepareLabels()
        nameTextField.delegate = self
        webpageTextField.delegate = self
        isFavouriteSwitch.isOn = false
        fillForEditing()
        preparePhotosCollection()
        
        imagePicker.delegate = self
        scrollView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveTextViewForKeyboard(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(moveTextViewForKeyboard(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: - Public methods for navigation
    
    func returningLocation(coordinates: CLLocationCoordinate2D) {
        choosedLocation = coordinates
        updateLocationButton(location: CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude))
    }

    //MARK: - Private Methods
    
    private func prepareNavBar() {
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneController))
        self.navigationItem.rightBarButtonItem = doneBtn
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelController))
        self.navigationItem.leftBarButtonItem = cancelBtn
    }
    
    private func prepareLabels() {
        nameLabel.text = NSLocalizedString("NAME_TITLE", comment: "Title of label describing name of place")
        let addLocationTitle = NSLocalizedString("ADDLOCATION_TITLE", comment: "Title of button for adding location")
        addLocationButton.setTitle(addLocationTitle, for: .normal)
        webpageLabel.text = NSLocalizedString("WEBPAGE_TITLE", comment: "Title of label describing webpage")
        isFavouriteLabel.text = NSLocalizedString("ISFAVOURITE_TITLE", comment: "Title of label describing is place favourite")
        noteTextView.text = NSLocalizedString("ADDNOTE_PLCHOLDER", comment: "Placeholder of textview for adding note to place")
    }
    
    private func preparePhotosCollection() {
        photosCollectionView.register(UINib(nibName: CreateVCConstants.photoCellNibName, bundle: Bundle.main) , forCellWithReuseIdentifier: CreateVCConstants.photoCellIdentifier)
        photosCollectionView.isScrollEnabled = true
        photosCollectionView.dataSource = self
        photosCollectionView.delegate = self
        
        addPhotoButton.addTarget(self, action: #selector(addPhotos), for: .touchDown)
    }
    
    private func fillForEditing() {
        if sourceVC != nil, let place = place {
            photosNames = Array(place.imageNames)
            nameTextField.text = place.name
            if let latitude = place.latitude, let longitude = place.longitude {
                choosedLocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                updateLocationButton(location: CLLocation(latitude: latitude, longitude: longitude))
            }
            webpageTextField.text = (place.webPage != nil) ? place.webPage : ""
            isFavouriteSwitch.isOn = place.isFavourite
            noteTextView.text = (place.note != nil) ? place.note : noteTextView.text
        }
    }
    
    private func updateLocationButton(location: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            if error == nil, let placemark = placemarks?.first {
                self.addLocationButton.setTitle(Tools.createLocationString(placemark: placemark), for: .normal)
            }
        }
    }
    
    @objc private func addPhotos() {
        let actionSheetTitle = NSLocalizedString("PHOTOS_TITLE_ALERT", comment: "Title for alert of adding photos to entry")
        let actionSheet = UIAlertController(title: actionSheetTitle, message: nil, preferredStyle: .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            actionSheet.addAction(createLibraryAction())
        }
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            actionSheet.addAction(createCameraAction())
        }
        
        let cancelTitle = NSLocalizedString("CANCEL_TITLE", comment: "Title for cancelation buttons")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        actionSheet.addAction(cancelAction)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    private func createLibraryAction() -> UIAlertAction {
        let libraryActionTitle = NSLocalizedString("PHOTOS_TITLE_LIBRARY", comment: "Title for action of adding photos from library to entry")
        let libraryAction = UIAlertAction(title: libraryActionTitle, style: .default) { (action) in
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == .authorized || status == .notDetermined {
                    self.imagePicker.allowsEditing = false
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                    DispatchQueue.main.async {
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                } else {
                    let noPermissionTitle = NSLocalizedString("NO_PERMISSION_TITLE", comment: "Title for action of no permission for access to device features e.g. camera")
                    let noLibraryPermMsg = NSLocalizedString("NO_PERMISSION_LIBRARY_MSG", comment: "Message for action of no permission for access to photo library")
                    DispatchQueue.main.async {
                        self.presentSimpleAlert(title: noPermissionTitle, msg: noLibraryPermMsg)
                    }
                }
            })
        }
        return libraryAction
    }
    
    private func createCameraAction() -> UIAlertAction {
        let cameraActionTitle = NSLocalizedString("PHOTOS_TITLE_CAMERA", comment: "Title for action of adding photos from camera to entry")
        let cameraAction = UIAlertAction(title: cameraActionTitle, style: .default) { (action) in
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                if granted {
                    self.imagePicker.allowsEditing = false
                    self.imagePicker.sourceType = .camera
                    self.imagePicker.cameraCaptureMode = .photo
                    DispatchQueue.main.async {
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        let noPermissionTitle = NSLocalizedString("NO_PERMISSION_TITLE", comment: "Title for action of no permission for access to device features e.g. camera")
                        let noCameraPermMsg = NSLocalizedString("NO_PERMISSION_CAMERA_MSG", comment: "Message for action of no permission for access to camera")
                        self.presentSimpleAlert(title: noPermissionTitle, msg: noCameraPermMsg)
                    }
                }
            })
        }
        return cameraAction
    }
    
    private func presentSimpleAlert(title: String?, msg: String?) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func moveTextViewForKeyboard(notification: Notification) {
        switch notification.name {
        case NSNotification.Name.UIKeyboardWillShow:
            adjustScrollForKeyboard(show: true, notification: notification)
        case NSNotification.Name.UIKeyboardWillHide:
            adjustScrollForKeyboard(show: false, notification: notification)
        default:
            return
        }
    }
    
    private func adjustScrollForKeyboard(show: Bool, notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let adjustment = (keyboardFrame.height * (show ? 1.0 : -1.0))
        scrollView.contentInset.bottom += adjustment
        scrollView.scrollIndicatorInsets.bottom += adjustment
    }
    
    @objc private func doneController() {
        let access = DataAccessPlace()
        self.notAcceptedPhotosNames = []
        let noNameAlert = {
            let alertTitle = NSLocalizedString("NONAME_ALERT_TITLE", comment: "Title for alert when name for place is not set")
            let alertMsg = NSLocalizedString("NONAME_ALERT_MSG", comment: "Message for alert when name for place is not set")
            self.presentSimpleAlert(title: alertTitle, msg: alertMsg)
        }
        if let sourceVC = sourceVC, let place = place {
            access.updatePlace(place, updateBlock: { (place) in
                if let name = self.nameTextField.text, name.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    place.name = name
                } else {
                    noNameAlert()
                    return
                }
                place.imageNames.removeAll()
                self.setPlaceData()
                sourceVC.editEnded()
                self.dismiss(animated: true, completion: nil)
            })
        } else {
            if let name = self.nameTextField.text, name.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                place = access.createPlace(name: name)
                self.setPlaceData()
                access.savePlace(place!)
                self.dismiss(animated: true, completion: nil)
            } else {
                noNameAlert()
                return
            }
        }
    }
    
    private func setPlaceData() {
        if let place = place {
            place.imageNames.append(objectsIn: photosNames)
            if let latitude = choosedLocation?.latitude, let longitude = choosedLocation?.longitude {
                place.latitude = latitude
                place.longitude = longitude
            }
            if let webpage = self.webpageTextField.text, webpage.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                place.webPage = webpage
            }
            let notePlaceholder = NSLocalizedString("ADDNOTE_PLCHOLDER", comment: "Placeholder of textview for adding note to place")
            if let note = self.noteTextView.text, note != notePlaceholder, note.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                place.note = note
            }
            place.isFavourite = self.isFavouriteSwitch.isOn
        }
    }
    
    @objc private func cancelController() {
        self.view.endEditing(true)
        self.notAcceptedPhotosNames.forEach { Tools.removeImage(name: $0) }
        self.notAcceptedPhotosNames = []
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Actions
    
    @IBAction func addLocationTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: CreateVCConstants.chooseLocationSegueIdentifier, sender: self)
    }
    
    
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == CreateVCConstants.chooseLocationSegueIdentifier, let nav = segue.destination as? UINavigationController, let dst = nav.topViewController as? ChooseLocationViewController {
            dst.choosedLocation = choosedLocation
            dst.sourceVC = self
        }
    }
}

extension CreateViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateVCConstants.photoCellIdentifier, for: indexPath) as! PhotoCollectionViewCell
        
        cell.image = Tools.readJPG(name: photosNames[indexPath.row])
        
        return cell
    }
}

extension CreateViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageName = "\(Date())-IMG\(self.photosNames.count)"
            Tools.saveToJPG(image: image, name: imageName)
            self.photosNames.append(imageName)
            self.notAcceptedPhotosNames.append(imageName)
            self.photosCollectionView.insertItems(at: [IndexPath(item: photosNames.count-1, section: 0)])
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension CreateViewController: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if velocity.y < 0 {
            self.view.endEditing(true)
        }
    }
    
}

extension CreateViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
