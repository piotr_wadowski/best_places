//
//  DataAccessPlaceTests.swift
//  BestPlacesTests
//
//  Created by Piotr Wadowski on 23.12.2017.
//  Copyright © 2017 Piotr Wadowski. All rights reserved.
//

import XCTest
@testable import BestPlaces
import RealmSwift

class DataAccessPlaceTests: XCTestCase {
    
    let db = try! Realm()
    var access: DataAccessPlace?
    let samplePlace = Place(idx: 0, name: "Cool place")
    
    override func setUp() {
        super.setUp()
        
        try! db.write {
            db.deleteAll()
        }
        access = DataAccessPlace()
    }
    
    override func tearDown() {
        try! db.write {
            db.deleteAll()
        }
        access = nil
        
        super.tearDown()
    }
    
    func testGetAllPlaces() {
        // Given
        addTestPlace()
        
        // When
        let places = access!.places
        
        // Then
        XCTAssertEqual(places, [samplePlace])
    }
    
    func testGetPlacesWithLocation() {
        // Given
        let testPlace = Place(idx: 1, name: "bbb")
        testPlace.latitude = 10.0
        testPlace.longitude = 10.0
        try! db.write {
            db.add(Place(idx: 0, name: "aaa"))
            db.add(testPlace)
        }
        
        // When
        let places = access!.placesWithLocation
        
        // Then
        XCTAssertEqual(places.count, 1)
        XCTAssertNotNil(places[0].latitude)
        XCTAssertNotNil(places[0].longitude)
    }
    
    func testCreatePlace() {
        // Given
        addTestPlace()
        
        // When
        let newPlace = access!.createPlace(name: "Greate place")
        
        // Then
        XCTAssertEqual(newPlace.idx, db.objects(Place.self).count)
    }
    
    func testMovePlaceUp() {
        // Given
        let testPlace = Place(idx: 3, name: "ddd")
        let places = [
            Place(idx: 0, name: "aaa"),
            Place(idx: 1, name: "bbb"),
            Place(idx: 2, name: "ccc"),
            testPlace,
            Place(idx: 4, name: "eee"),
            Place(idx: 5, name: "fff")
        ]
        try! db.write {
            db.add(places)
        }
        
        // When
        access!.movePlace(testPlace, newIdx: 1)
        
        // Then
        for (idx, place) in db.objects(Place.self).sorted(byKeyPath: "idx").enumerated() {
            if idx == 1 {
                XCTAssertEqual(place.name, testPlace.name)
            }
            XCTAssertEqual(idx, place.idx)
        }
    }
    
    func testMovePlaceDown() {
        // Given
        let testPlace = Place(idx: 1, name: "bbb")
        let places = [
            Place(idx: 0, name: "aaa"),
            testPlace,
            Place(idx: 2, name: "ccc"),
            Place(idx: 3, name: "ddd"),
            Place(idx: 4, name: "eee"),
            Place(idx: 5, name: "fff")
        ]
        try! db.write {
            db.add(places)
        }
        
        // When
        access!.movePlace(testPlace, newIdx: 3)
        
        // Then
        for (idx, place) in db.objects(Place.self).sorted(byKeyPath: "idx").enumerated() {
            if idx == 3 {
                XCTAssertEqual(place.name, testPlace.name)
            }
            XCTAssertEqual(idx, place.idx)
        }
    }
    
    func testSavingPlace() {
        // Given
        
        // When
        access!.savePlace(samplePlace)
        
        //Then
        let numOfPlaces = db.objects(Place.self).count
        XCTAssertEqual(numOfPlaces, 1)
    }
    
    func testUpdatePlace() {
        // Given
        addTestPlace()
        let newNote = "It is great place to eat!"
        
        // When
        access!.updatePlace(samplePlace) { (place) in
            place.note = newNote
        }
        
        // Then
        XCTAssertEqual(newNote, db.objects(Place.self).first?.note)
    }
    
    func testDeletePlace() {
        // Given
        addTestPlace()
        // When
        access!.deletePlace(samplePlace)
        
        // Then
        XCTAssert(db.objects(Place.self).isEmpty)
    }
    
    private func addTestPlace() {
        try! db.write {
            db.add(samplePlace)
        }
    }
    
}
